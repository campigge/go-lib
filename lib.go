package fowlit

import (
	"bytes"
	"crypto/sha512"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/go-chi/render"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"log"
)

type Route struct {
	Path       string  `json:"path"`
	Method     string  `json:"method"`
	Permission *string `json:"permission"`
}

type JSONError struct {
	Status    int       `json:"status"`
	Time      time.Time `json:"time"`
	ErrorCode uint32    `json:"errorCode,string"`
}

type Error struct {
	Code   int
	Err    error
	Method string
}

func New(method string) Error {
	return Error{Method: method}
}

func (e Error) Wrap(fErr *Error, msg string) *Error {
	return e.Set(fErr.Code, errors.Wrap(fErr.Error(), msg))
}

func (e Error) Use(fErr *Error) *Error {
	return e.Set(fErr.Code, fErr.Error())
}

func (e Error) Set(code int, err error) *Error {
	e.Code = code
	e.Err = err
	return &e
}

func (e Error) Error() error {
	return errors.WithStack(errors.Wrap(e.Err, e.Method))
}

func HandleError(w http.ResponseWriter, e *Error) {
	err := e.Error()
	var errStatus = rand.Uint32()
	if err != nil {
		log.Printf("[Error] %-10d %v\n", errStatus, err)
	}

	var lt = JSONError{
		Time:      time.Now(),
		Status:    e.Code,
		ErrorCode: errStatus,
	}
	w.WriteHeader(e.Code)

	b, err := json.Marshal(lt)
	if err != nil {
		log.Panic(err)
		return
	}
	_, _ = w.Write(b)
}

func Seed() {
	rand.Seed(time.Now().UnixNano())
}

type HandlerFunc func(http.ResponseWriter, *http.Request) *Error

func (f HandlerFunc) ServeHTTP(w http.ResponseWriter, r *http.Request) *Error {
	return f(w, r)
}

// Hash a two strings
func HashString(str string) string {
	h := sha512.New()
	h.Write([]byte(str))
	return base64.URLEncoding.EncodeToString(h.Sum(nil))
}

// Generate a random string of fixed size
func GetRandomString(size int) string {
	var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, size)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// Generate a random string of fixed size
func GenID(size int) string {
	var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyz")
	b := make([]rune, size)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// Split string on n:th occurrence of delimiter
func SplitOnN(str string, del rune, num int) ([]string, error) {
	counter := 0
	for i, char := range str[:len(str)-1] {
		if char == del {
			if counter == num {
				return []string{str[:i], str[i+1:]}, nil
			}
			counter++
		}
	}
	return nil, fmt.Errorf("Delimiter: '" + string(del) + "' was not found")
}

// Send a JSON Request
func SendJSONRequest(url string, method string, jsonBody interface{}, end interface{}) (int, error) {
	var body []byte
	var err error
	// JSON encode request
	if jsonBody != nil {
		body, err = json.Marshal(jsonBody)
		if err != nil {
			return 0, err
		}
	}

	// Create requestgitlab.com/campigge/ibex
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return 0, err
	}
	req.Header.Set("Content-Type", "application/json")

	// Send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}

	// Parse response
	if end != nil {
		if err = render.DecodeJSON(resp.Body, end); err != nil {
			return 0, err
		}
	}

	return resp.StatusCode, nil
}

func GetEnv(env string) string {
	val := os.Getenv(env)
	if len(val) == 0 {
		log.Println(env + " is not defined")
	}
	return val
}

func GetIntEnv(env string) int {
	val := os.Getenv(env)
	if len(val) == 0 {
		log.Println(env + " is not defined")
	}
	ret, err := strconv.Atoi(val)
	if err != nil {
		log.Println(env + " is not a number")
	}
	return ret
}

func ToJpg(imageBytes []byte) ([]byte, error) {
	contentType := http.DetectContentType(imageBytes)
	fmt.Println(contentType)
	switch contentType {

	case "image/jpeg":
		return imageBytes, nil
	case "image/png":
		img, err := png.Decode(bytes.NewReader(imageBytes))
		if err != nil {
			return nil, errors.Wrap(err, "unable to decode png")
		}

		buf := new(bytes.Buffer)
		if err := jpeg.Encode(buf, img, nil); err != nil {
			return nil, errors.Wrap(err, "unable to encode jpeg")
		}

		return buf.Bytes(), nil
	}

	return nil, fmt.Errorf("unable to convert %#v to jpeg", contentType)
}

func LoadImage(path string, id int64) ([]byte, error) {
	// Read the users profile picture from the system
	fileName := path + strconv.FormatInt(id, 10) + ".jpg"
	img, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}
	return img, nil
}

func DeleteImage(path string, id int64) error {
	// Read the users profile picture from the system
	fileName := path + strconv.FormatInt(id, 10) + ".jpg"
	// Remove the profile picture
	err := os.Remove(fileName)
	if err != nil {
		return err
	}
	return nil
}

func SaveImage(path string, id int64, img []byte) error {
	fileName := path + strconv.FormatInt(id, 10) + ".jpg"

	err := ioutil.WriteFile(fileName, img, 0644)
	if err != nil {
		return errors.Wrap(err, "could not save image.")
	}
	return nil
}

func DaysBetween(a, b time.Time) int {
	if a.After(b) {
		a, b = b, a
	}

	days := -a.YearDay()
	for year := a.Year(); year < b.Year(); year++ {
		days += time.Date(year, time.December, 31, 0, 0, 0, 0, time.UTC).YearDay()
	}
	days += b.YearDay()

	return days
}
